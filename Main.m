%Clearing all memory and closing all figures 
clear all;
close all;
%____________________Do not touch___________________________________

yes = 1;
no = 0;

%_________________Relativistic factors_____________________________

rel_velocity = evalin('base', 'rel_velocity'); %Velocity of the relative frames
vframe = [rel_velocity,0];        
AddFactors = evalin('base', 'AddFactors');%Would you like to add relativistic factors (single pendulum)?

%__________________Pendulum Main Console____________________________
%Please select and option which you would like to run
%click = 1 : Single Pendulum with Eulers Method.
%click = 2 : Single Pendulum with Runge Kutta Method.
%click = 3 : Double Pendulum with Eulers Method 
%click = 4 : Double Pendulum with Runge Kutta 4th Order Method
%click = 5 : Single Pendulum in 3 Dimensions Runge Kutta 4th Order Method
%click = 6 : Single Forced damped Pendulum Runge Kutta Method
%click = 7 : Double Pendulum with Relativistic factors Runge Kutta 4th method 
click = evalin('base','click'); %sets click from GUI
%% 

%_________________Single Pendulum & 3D Single Pendulum_____________________

%Pendulum settings 

%  g = 9.81;        %Acceleration due to Gravity 
% L1 = 10;          %length of the string
% theta = pi/2;  %initial angle of release
% psi3 = pi/3;     %initial angle of release
% dtheta = 0;      %The initial angular velocity dtheta/dt
% dpsi = -4;       %The initial angular velocity dpsi/dt (3D)
%%

%%___________________Pendulum Animation____________________________________
% 
radiusSP = 0.1;             %The radius of the mass of Single Pendulum
radius1 = 0.1;              %Radius of the first mass of the double pendulum
radius2 = 0.1;              %Radius of the second mass of the double pendulum


Animation = evalin('base', 'Animation'); %Animation on?
RelativityAnimation = evalin('base', 'RelativityAnimation');  %Animation for relativity?

%% 
                        %Poincare and Phase plotting                
                        
%To what accuracy? (decimal places)
accuracy = evalin('base','accuracy'); %accuracy - decimal placing of theta
%What would you like to plot? (Pick one)
Poincare =  evalin('base','Poincare');
Poincare3D = evalin('base','Poincare3D');
PhasePlot = evalin('base','PhasePlot');
PhasePlotin3D = evalin('base','PhasePlotin3D');

%What should be the condition?
Condition = 0;

Theta = evalin('base','Theta');
PSI = evalin('base','PSI');
ThetaDOT = evalin('base','ThetaDOT');
PsiDOT = evalin('base','PsiDOT');

%Which variables should be plotted?
OnTheta = evalin('base','OnTheta');
OnPsi = evalin('base','OnPsi');
OnThetaDOT = evalin('base','OnThetaDOT');
OnPsiDOT = evalin('base','OnPsiDOT');

% In the phase Diagram would you like to crop the plotting to fit in a
% domain? (-pi -> pi)
CropTheta = yes;
CropPsi = yes;
%% 
%Pendulum settings

%Time parameters
t = 0;      %Start time 
h = evalin('base','h');  %Step size 
TEnd = evalin('base','TEnd'); %End time 
 

%Single Pendulum

g = evalin('base', 'g'); %Acceleration due to Gravity
L1 = evalin('base', 'L1'); %length of the string
theta = evalin('base', 'theta'); %initial angle of release 
dtheta = evalin('base', 'dtheta');   %The initial angular velocity dtheta/dt 
psi3 = evalin('base', 'psi3'); %The initial angular velocity dtheta/dt
dpsi = evalin('base', 'dpsi'); %The initial angular velocity dpsi/dt (3D)

%Single Forced Damped Pendulum

q = evalin('base', 'q'); %Damping factor of the pendulum 
theta2 = evalin('base', 'theta2'); %Initial angle of release 
thetadot = evalin('base', 'thetadot');%Initial veloctiy of the pendulum 
g1 = evalin('base', 'g1'); %Forcing Amplitude
psi = evalin('base', 'psi'); %Frequency of the applied force 
        
% Double Pendulum 

theta1 = evalin('base', 'theta1'); %Initial angle of the first rod
psi1 = evalin('base', 'psi1');   %Initial angle of the second rod 
l1 = evalin('base', 'l1');         %Length of the first rod 
l2 = evalin('base', 'l2');         %Length of the second rod 
m1 = evalin('base', 'm1');         %Mass of the first bulb 
m2 = evalin('base', 'm2');         %Mass of the second bulb 
v = evalin('base', 'v');          %Angular velocity of theta
u = evalin('base', 'u');          %Angular velocity of psi
%% 
%================inputs for testing without GUI============================
%________________Single Forced Damped Pendulum_____________________________

% q = 2;          %Damping factor of the pendulum 
% theta2 = pi/10; %Initial angle of release 
% thetadot = 0;   %Initial veloctiy of the pendulum 
% g1 = 1.15;       %Forcing Amplitude
% psi = (2/3)*pi;      %Frequency of the applied force 

%_____________________Double Pendulum______________________________________
% theta1 = (179/180)*pi ;%Initial angle of the first rod
% x = 179;
% psi1 = x*pi/180 ;   %Initial angle of the second rod 
% l1 = 1;         %Length of the first rod 
% l2 = 1;         %Length of the second rod 
% m1 = 1;         %Mass of the first bulb 
% m2 = 1;         %Mass of the second bulb 
% v = 0;          %Angular velocity of theta
% u = 0;          %Angular velocity of psi

% %Time parameters
% t = 0;      %Start time 
% h = 0.01;  %Step size 
% TEnd = 10; %End time 
%% 
%Number of steps to be computed
N = TEnd/h;

%% 
%Eulers Method on the single 2D pendulum 
if click == 1;
       
    openfig('plots.fig')  
    
    for i = 1:N
        
    %Plot Labeling
    if Animation == yes
        title('Single Pendulum Eulers Method')
        xlabel('x (m)')
        ylabel('y (m)')
        axis([-L1*1.1 L1*1.1 -L1*1.1 L1*1.1])
        axis square 
    elseif PhasePlot == yes
        title('2D Phase Plot of Single Pendulum Eulers Method')
        xlabel('\phi') 
        ylabel('$\dot{\theta}$','interpreter','latex')
    elseif PhasePlotin3D == yes
        title('3D Phase Plot of Single Pendulum Eulers Method')
        xlabel('\phi') 
        ylabel('$\dot{\theta}$','interpreter','latex')
        zlabel('$\dot{\phi}$','interpreter','latex')
    end
    
    %Loop end condition 
    if i == N
        break
    end
         
    %Time counter 
    t = t + h;
    
    %Convert to Cartisian coordinates using the conversion function 
    [x,y,vx,vy] = Cartesian(L1,0,theta,0,dtheta,0);
        
    if AddFactors == yes
        %This is calling the relativistic conversion factors
        [LP,mP,hP,thetaP,dthetaP] = RelativisticFactors(L1,vframe,0,h,theta,dtheta);
        
        %Converting to cartesian coordinates
        [xP,yP] = Cartesian(LP,0,thetaP,0,dthetaP,0);
    end 
     
     
    %Decision for poincare
        if Theta == 1
            X = theta;
            angle = yes;
        elseif ThetaDOT == 1
            X = dtheta;
            angle = no;
        end 
        
        %Poincare and Phase Plotting
        [theta,~,dtheta,~] = PoincareAndPhaseDiagram(theta,OnTheta,CropTheta,0,0,0,dtheta,OnThetaDOT,0,0,X,angle,Condition,PhasePlot,no,Poincare,no,accuracy);
         
     %Animation of the Pendulum 
     if Animation == yes 
         if RelativityAnimation == yes
             
             %Animation inserted as a function
             AnimationOfPendulum(xP,yP,0,0,radiusSP,0,L1,0,1,hP)
         else
             %Animation without relativity
             AnimationOfPendulum(x,y,0,0,radiusSP,0,L1,0,1,h)
             
         end
     end
     
     
    %Calling the single pendulum 2 Dimensions function 
    [q,yprime] = SinglePendulum2D(g,L1,theta,dtheta); 
    
    %Calling the Euler Method to get the dtheta/dt
    [dtheta] = EulerMethod(h,yprime,q);
    
    %Calling the Euler Method to get the theta
    [theta] = EulerMethod(h,dtheta,theta);
    
    end 
end 
%% 

%Runge Kutta method on the single 2D pendulum 
if click == 2
    
   openfig('plots.fig') 
    
    for i = 1:N
    
%Plot Labeling
    if Animation == yes
        title('Single Pendulum Runge Kutta Method')
        xlabel('x (m)')
        ylabel('y (m)')
        axis([-L1*1.1 L1*1.1 -L1*1.1 L1*1.1])
        axis square 
    end
    
    %Loop end condition 
    if i == N
        break
    end
        
    %Convert to Cartisian coordinates using the conversion function 
    [x,y,vx,vy] = Cartesian(L1,0,theta,0,dtheta,0);
    
    if AddFactors == yes
        %This is calling the relativistic conversion factors
        [LP,mP,~,thetaP,dthetaP] = RelativisticFactors(L1,vframe,0,0,theta,dtheta);
        
        %Converting to cartesian coordinates
        [xP,yP] = Cartesian(LP,0,thetaP,0,dthetaP,0);
        
    end 
    
     %Animation of the Pendulum 
     if Animation == yes 
         if RelativityAnimation == yes
             
             %Animation inserted as a function
             AnimationOfPendulum(xP,yP,0,0,radiusSP,0,L1,0,1,h)
         else
             %Animation without relativity
             AnimationOfPendulum(x,y,0,0,radiusSP,0,L1,0,1,h)
         end
     end 
        
    %Time counter
    t = t + h;
    
    %Calling the single pendulum 2 Dimensions function
    [dtheta,yprime] = SinglePendulum2D(g,L1,theta,dtheta);
    
    %Calling the Runge Kutta 4th order to get dtheta/dt
    [dtheta] = RungeKutta4th(h,yprime,dtheta);
    
    %Calling the Runge Kutta 4th order to get theta
    [theta] = RungeKutta4th(h,dtheta,theta);
 
    end
end
%% 
%Eulers method for the double pendulum in 2D
if click == 3
           
    openfig('plots.fig')  

    for i = 1:N
        
    %Plot Labeling    
    if Animation == yes
        title('Double Pendulum Eulers Method')
        xlabel('x (m)')
        ylabel('y (m)')
        axis([-(l1+l2)*1.1 (l1+l2)*1.1 -(l1+l2)*1.1 (l1+l2)*1.1])
        axis square 
    elseif PhasePlot == yes
        title('2D Phase Plot of Double Pendulum Eulers Method')
        xlabel('\phi') 
        ylabel('$\dot{\theta}$','interpreter','latex')
    elseif PhasePlotin3D == yes
        title('3D Phase Plot of Double Pendulum Eulers Method')
        xlabel('\phi') 
        ylabel('$\dot{\theta}$','interpreter','latex')
        zlabel('$\dot{\phi}$','interpreter','latex')
    elseif Poincare == yes
        title('2D Poincare Map of Double Pendulum Eulers Method')
        xlabel('\phi') 
        ylabel('$\dot{\theta}$','interpreter','latex')
    elseif Poincare3D == yes
        title('3D Poincare Map of Double Pendulum Runge Eulers Method')
        xlabel('\phi') 
        ylabel('$\dot{\theta}$','interpreter','latex')
        zlabel('$\dot{\phi}$','interpreter','latex')
    end
    
    %Loop end condition 
 
    if i == N
        break
    end
 
    %Convert to Cartisian coordinates using the conversion function 
    [x1,y1,vx1,vy1] = Cartesian(l1,0,theta1,0,v,0);
    
    %Converting the position of mass 2 in cartesian coordinates  
    [x2,y2,vx2,vy2] = Cartesian(l1,l2,theta1,psi1,v,u);
    
    %Decision for the poincare and Phase diagram
        if Theta == 1
            X = theta1;
            angle = yes;
        elseif PSI == 1
            X = psi1;
            angle = yes;
        elseif ThetaDOT == 1
            X = v;
            angle = no;
        elseif PsiDOT == 1
            X = u;
            angle = no;
        end 

    %Plotting of the Poincare and Phase
    [theta1,psi1,v,u] = PoincareAndPhaseDiagram(theta1,OnTheta,CropTheta,psi1,OnPsi,CropPsi,v,OnThetaDOT,u,OnPsiDOT,X,angle,Condition,PhasePlot,PhasePlotin3D,Poincare,Poincare3D,accuracy); 

    %Animation of the Pendulum 
     if Animation == yes 
         
         %Animation without relativity
         AnimationOfPendulum(x1,y1,x2,y2,radius1,radius2,l1,l2,2,h)

     end
    
    %Time counter
    t = t + h;
    
    %This is the double Pendulum function being called into the main 
    [v,u,vprime,uprime] = DoublePendulum2D(m1,m2,l1,l2,theta1,psi1,v,u,g);
    
    %Solving for dtheta/dt
    [v] = EulerMethod(h,vprime,v);
    
    %Solving for dpsi/dt
    [u] = EulerMethod(h,uprime,u);
    
    %Solving for the angle psi
    [psi1] = EulerMethod(h,u,psi1);
    
    %Solving for the angle theta
    [theta1] = EulerMethod(h,v,theta1);
    
   
    end 
  
end 

%% 
%Runge Kutta Method for the Double Pendlum 
if click == 4

    openfig('plots.fig')
    
    for i = 1:N
    
    %Plot Labeling    
    if Animation == yes
        title('Double Pendulum Runge Kutta Method')
        xlabel('x (m)')
        ylabel('y (m)')
        axis([-(l1+l2)*1.1 (l1+l2)*1.1 -(l1+l2)*1.1 (l1+l2)*1.1])
        axis square 
    elseif PhasePlot == yes
        title('2D Phase Plot of Double Pendulum Runge Kutta Method')
        xlabel('\phi') 
        ylabel('$\dot{\theta}$','interpreter','latex')
    elseif PhasePlotin3D == yes
        title('3D Phase Plot of Double Pendulum Runge Kutta Method')
        xlabel('\phi') 
        ylabel('$\dot{\theta}$','interpreter','latex')
        zlabel('$\dot{\phi}$','interpreter','latex')
    elseif Poincare == yes
        title('2D Poincare Map of Double Pendulum Runge Kutta Method')
        xlabel('\phi') 
        ylabel('$\dot{\theta}$','interpreter','latex')
    elseif Poincare3D == yes
        title('3D Poincare Map of Double Pendulum Runge Kutta Method')
        xlabel('\phi') 
        ylabel('$\dot{\theta}$','interpreter','latex')
        zlabel('$\dot{\phi}$','interpreter','latex')
    end
    
    %Loop end condition 
    if i == N
        break
    end
        
        %Converting the position of mass 1 to Cartisian coordinates
        [x1,y1,vx1,vy1] = Cartesian(l1,0,theta1,0,v,0);
    
        %Converting the position of mass 2 in cartesian coordinates  
        [x2,y2,vx2,vy2] = Cartesian(l1,l2,theta1,psi1,v,u);
        
        %Time counter
        t = t + h;
        
        %Decision for the poincare and Phase diagram
        if Theta == 1
            X = sin(theta1);
            angle = yes;
        elseif PSI == 1
            X = psi1;
            angle = yes;
        elseif ThetaDOT == 1
            X = v;
            angle = no;
        elseif PsiDOT == 1
            X = u;
            angle = no;
        end 
        
        %Plotting of the Poincare and Phase
        [theta1,psi1,v,u] = PoincareAndPhaseDiagram(theta1,OnTheta,CropTheta,psi1,OnPsi,CropPsi,v,OnThetaDOT,u,OnPsiDOT,X,angle,Condition,PhasePlot,PhasePlotin3D,Poincare,Poincare3D,accuracy); 
        
     %Animation of the Pendulum 
     if Animation == yes 
         
         %Animation without relativity
         AnimationOfPendulum(x1,y1,x2,y2,radius1,radius2,l1,l2,2,h)
         
     end
        
        %This is calling the double Pendulum function 
        [v,u,vprime,uprime] = DoublePendulum2D(m1,m2,l1,l2,theta1,psi1,v,u,g);
        
        %Solving for dtheta/dt
        [v] = RungeKutta4th(h,vprime,v);
        
        %Solving for dpsi/dt
        [u] = RungeKutta4th(h,uprime,u);
        
        %Solving for theta
        [theta1] = RungeKutta4th(h,v,theta1);
        
        %Solving for psi 
        [psi1] = RungeKutta4th(h,u,psi1);
          
    end 
end



%% 
%Single Pendulum in 3 Dimensions 
if click == 5 
   openfig('plots.fig') 
  
   for i = 1:N
    if Animation == yes
        title('Single Pendulum ')
        xlabel('x (m)')
        ylabel('y (m)')
        zlabel('z (m)')
        axis([-L1*1.1 L1*1.1 -L1*1.1 L1*1.1 -L1*1.1 L1*1.1])
        axis square
    end
     
    %Loop end condition 
    if i == N
        break
    end
        
        %Calling the function of the Single 3D pendulum 
        [dtheta,uprime,x,y,z] = SimplePendulum3D(g,L1,t,theta,psi3,dtheta,dpsi);
        
        %Time counter
        t = t+h;
        
        %Solving for dtheta/dt using RK4th
        [dtheta] = RungeKutta4th(h,uprime,dtheta);
        
        %Solving for theta using RK4th
        [theta] = RungeKutta4th(h,dtheta,theta);
        
        %Calling animation of 3D pendulum
        AnimationOfPendulum3D(x,y,z,.1,2,h,L1)
    end 
   
end 

%%
%Forced Damped single Pendulum 
if click ==6
    openfig('plots.fig')
    for i = 1:N
      
%Plot Labeling
    if Animation == yes
        title('Forced Single Pendulum')
        xlabel('x (m)')
        ylabel('y (m)')
        axis([-L1*1.1 L1*1.1 -L1*1.1 L1*1.1])
        axis square 
    end
    %Loop end condition 
    if i == N
        break
    end
        
        %Converting to cartesian coordinates 
        [x,y,vx,vy] = Cartesian(1,0,theta2,0,thetadot,0);
        
        %Time counter 
        t = t + h;
        
        %Decision for poincare
        if Theta == 1
            X = theta2;
            angle = yes;
        elseif ThetaDOT == 1
            X = thetadot;
            angle = no;
        elseif PsiDOT == 1
            X = cos(psi*t);
            angle = no;
        end 
        
        %Poincare and Phase Plotting
        [theta2,~,thetadot,psi] = PoincareAndPhaseDiagram(theta2,OnTheta,CropTheta,0,0,0,thetadot,OnThetaDOT,psi,OnPsiDOT,X,angle,Condition,PhasePlot,PhasePlotin3D,Poincare,Poincare3D,accuracy);
        
        %Animation of the Pendulum 
        if Animation == yes 
             
             %Animation without relativity
             AnimationOfPendulum(x,y,0,0,radiusSP,0,L1,0,1,h)
             
        end
        
        %Calling the Forced damped Pendulum function
        [thetadot,uprime] = ForcedDampedSinglePendulum(g1,q,theta2,thetadot,psi,t);
        
        %Solving for dtheta/dt using the RK4th order method
        [thetadot] = RungeKutta4th(h,uprime,thetadot);
        
        %Solving for theta usign the RK4th order method
        [theta2] = RungeKutta4th(h,thetadot,theta2);
        
    end 
end
%%

%Double Pendulum Relativistic Runge Kutta
if click == 7
    
   openfig('plots.fig')
   
    for i = 1:N
    
        %plot labeling
    if Animation == yes
        title('Relativistic Double Pendulum')
        xlabel('x (m)')
        ylabel('y (m)')
        axis([-(l1+l2)*1.1 (l1+l2)*1.1 -(l1+l2)*1.1 (l1+l2)*1.1])
        axis square 
    elseif PhasePlot == yes
        title('2D Phase Plot of Relativistic Double Pendulum')
        xlabel('\phi') 
        ylabel('$\dot{\theta}$','interpreter','latex')
    elseif PhasePlotin3D == yes
        title('3D Phase Plot of Relativistic Double Pendulum')
        xlabel('\phi') 
        ylabel('$\dot{\theta}$','interpreter','latex')
        zlabel('$\dot{\phi}$','interpreter','latex')
    elseif Poincare == yes
        title('2D Poincare Map of Relativistic Double Pendulum')
        xlabel('\phi') 
        ylabel('$\dot{\theta}$','interpreter','latex')
    elseif Poincare3D == yes
        title('3D Poincare Map of Relativistic Double Pendulum')
        xlabel('\phi') 
        ylabel('$\dot{\theta}$','interpreter','latex')
        zlabel('$\dot{\phi}$','interpreter','latex')
    end
    
    %Loop end condition 
    if i == N
        break
    end
        
        %Time counter
        t = t + h;
       
        %Calling the relativistic conversion function (for m1)
        [l1P,m1P,~,thetaP,vP] = RelativisticFactors(l1,vframe,m1,h,theta1,v);
        
        %Calling the relativistic conversion function (for m2)
        [l2P,m2P,~,psiP,uP] = RelativisticFactors(l2,vframe,m2,h,psi1,u);
          
        %Decision for the poincare and Phase diagram
        if Theta == 1
            X = sin(thetaP);
            angle = yes;
        elseif PSI == 1
            X = psiP;
            angle = yes;
        elseif ThetaDOT == 1
            X = vP;
            angle = no;
        elseif PsiDOT == 1
            X = uP;
            angle = no;
        end 
        
        %Plotting of the Poincare and Phase
        [thetaP,psiP,vP,uP] = PoincareAndPhaseDiagram(thetaP,OnTheta,CropTheta,psiP,OnPsi,CropPsi,vP,OnThetaDOT,uP,OnPsiDOT,X,angle,Condition,PhasePlot,PhasePlotin3D,Poincare,Poincare3D,accuracy);   
        
        %Calling the double Pendlum function 
        [v,u,vprime,uprime] = DoublePendulum2D(m1P,m2P,l1,l2,theta1,psi1,v,u,g);
        
        %Calling Runge Kutta 4th order method (dtheta/dt)
        [v] = RungeKutta4th(h,vprime,v);
        
        %Calling Runge Kutta 4th order method (theta)
        [theta1] = RungeKutta4th(h,v,theta1);
        
        %Calling Runge Kutta 4th order method (dpsi/dt)
        [u] = RungeKutta4th(h,uprime,u);
        
        %Calling RungeKutta 4th order method (psi)
        [psi1] = RungeKutta4th(h,u,psi1);
        
        %Cartesian conversion of m1
        [x1,y1,~,~] = Cartesian(l1P,0,thetaP,0,vP,0);
        
        %Cartesian conversion of m2
        [x2,y2,~,~] = Cartesian(l1P,l2P,thetaP,psiP,vP,uP);     
        
     %Animation of the Pendulum 
     if Animation == yes 
         if RelativityAnimation == yes
             
             %Animation inserted as a function
             AnimationOfPendulum(x1,y1,x2,y2,radius1,radius2,l1,l2,6,h)

         end
      end
   end 
end 