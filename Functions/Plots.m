function varargout = Plots(varargin)
% PLOTS MATLAB code for Plots.fig
%      PLOTS, by itself, creates a new PLOTS or raises the existing
%      singleton*.
%
%      H = PLOTS returns the handle to a new PLOTS or the handle to
%      the existing singleton*.
%
%      PLOTS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PLOTS.M with the given input arguments.
%
%      PLOTS('Property','Value',...) creates a new PLOTS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Plots_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Plots_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Plots

% Last Modified by GUIDE v2.5 02-Jun-2016 22:19:41

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Plots_OpeningFcn, ...
                   'gui_OutputFcn',  @Plots_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT
%________Loop to enable pause of animation with user input_________________
freeze = 1;
while (evalin('base', 'freeze')==0)      
pause(1)
end
%__________________________________________________________________________

% --- Executes just before Plots is made visible.
function Plots_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Plots (see VARARGIN)

% Choose default command line output for Plots
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Plots wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Plots_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

%__________________________________________________________________________
% --- Executes on button press in pushbutton1.
%set freeze to 0 in order to pause the animation
function pushbutton1_Callback(hObject, eventdata, handles)
freeze = 0;
 assignin('base','freeze',freeze);

% --- Executes on button press in pushbutton2.
%set freeze to 1 in order to resume the loop
function pushbutton2_Callback(hObject, eventdata, handles)
freeze = 1;
assignin('base','freeze',freeze);

% --- Executes on button press in btn_main_menu.
function btn_main_menu_Callback(hObject, eventdata, handles)
freeze = 0;
assignin('base','freeze',freeze); 
%pause animation

%set default click and return to main menu
click = 1;
assignin('base','click',click)

run('Main_Menu')%return to main menu

%__________________________________________________________________________
% --------------------------------------------------------------------
function uipushtool1_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to uipushtool1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
