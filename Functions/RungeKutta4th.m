function [yN2] = RungeKutta4th(h,yprime,yN1)
k1 = yprime;
k2 = yprime + (h/2)*k1;
k3 = yprime + (h/2)*k2;
k4 = yprime + h*k3;
yN2 = yN1 + (h/6)*(k1 + 2*k2 + 2*k3 +k4);
end