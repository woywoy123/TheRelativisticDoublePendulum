function [angle1,angle2,angledot1,angledot2] = PoincareAndPhaseDiagram(angle1,on1,crop1,angle2,on2,crop2,angledot1, on3,angledot2, on4,selectvariable,angleornot,condition,PhasePlot,plot33,Poincare,Poincare3,accuracy,i)

    %Preparing variables for use in poincare
%______________________________________________________________________
    if on1 == 1
        
        if crop1 == 1

            %Calling angle adjustment function for angle1
            angle1 = PhaseDiagram(angle1);
            var1 = angle1;
        
        else 
            var1 = angle1;
        end
        
        
    else
        var1 = 9999 ;
    end 

    if on2 == 1
        
        if crop2 == 1

        %Calling angle adjustment function for angle2
        angle2 = PhaseDiagram(angle2);
        var2 = angle2;
        else
            var2 = angle2;
       
        end 
        
    else 
        var2 = 9999 ;
        
    end 

    if on3 == 1
        
        var3 = angledot1;
    else 
        var3 = 9999 ;
        
    end 

    if on4 == 1
        
        var4 = angledot2;
    else 
        var4 = 9999 ;
        
    end 
    
    %The conditional variable (constraint)
    variable = selectvariable;
    if angleornot == 1
        var5 = PhaseDiagram(variable);
    else
        var5 = variable;
    end
    
%Collecting data 
Z(1,:) = [var1,var2,var3,var4];

B = Z;
B(Z == 9999)=[];


%__________________________________________________________________________

%Filtering the different cases

OnTot = on1 + on2 + on3 + on4;
if OnTot == 4;
    B(1,:) = [B(1,1),B(1,2),B(1,3),B(1,4)];
end 

if OnTot == 3;
    B(1,:) = [B(1,1),B(1,2),B(1,3)];
end 

if OnTot == 2;
    B(1,:) = [B(1,1),B(1,2)];
end 


%__________________________________________________________________________

        
%Phase Plot
    if PhasePlot == 1
        %Ph = [B(:,1),B(:,2)];
        plot(B(:,1),B(:,2),'b.','markersize',0.5)
        hold on
        drawnow
    end
    
%A 3D plot of the data 
    if plot33 == 1 
        %Ph = [B(:,1),B(:,2),B(:,3)];
        plot3(B(:,1),B(:,2),B(:,3),'r.','markersize',0.5)
        hold on
        drawnow
    end

% A Poincare plot 
    if Poincare == 1
        
        %accuracy refers to decimal placing this function rounds the number
        D = round(var5,accuracy);
        
        if D == condition 
            plot(B(:,1),B(:,2),'b.','markersize',0.5)
            hold on
            drawnow
        end 
    end 
     
% A 3D Poincare plot 
    if Poincare3 == 1
        
        %Accuracy refers to decimal placing 
        D = round(var5,accuracy);
        
        if D == condition
            plot3(B(:,1),B(:,2),B(:,3),'r.','markersize',0.5)
            hold on
            drawnow

        end 
    end 
end 





