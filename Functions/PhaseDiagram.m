function [adjustangle] = PhaseDiagram(angle)
    
    %This will shift the phase forward by 2pi 
    if angle <= -pi
        angle = angle + pi*2;
    end 
    
    %This will shift the pahse backwards by 2pi
    if angle >= pi
        angle = angle - pi*2;
    end 
    
    %Just outputting the data 
    adjustangle = angle;
end 