function [yN2] = EulerMethod(h,yprime,yN1)
%Performing the Euler Method
yN2 = yN1 + h*yprime;
end 

