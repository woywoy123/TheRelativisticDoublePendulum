function [y,yprime] = SinglePendulum2D(g,L,theta,thetadot)

%yprime is the second derivative of theta with time d^2(theta)/dt^2
yprime = -g*sin(theta)/L;

%y is the first derivative of theta dtheta/dt
y = thetadot; 

end 
