function [x,y,vx,vy] = Cartesian(L1,L2,theta,psi,thetadot,psidot)

%This function is the conversion for all pendulum functions to cartessian
%except for 3D single pendulum
%L1 is the length of the first string 
%L2 is the length of the second string 
%theta is the firt angle 
%psi is the second angle 


%Position vector
r(1,:) = [L1*sin(theta)+L2*sin(psi),-L1*cos(theta)-L2*cos(psi)];

%x position of pendulum 
x = r(1,1);

%y position of pendulum 
y = r(1,2);

%Velocity as a vector
v(1,:) = [L1*cos(theta)*thetadot + L2*cos(psi)*psidot,...
    L1*sin(theta)*thetadot + L2*sin(psi)*psidot];

% Velocity in the x direction
vx = v(1,1);

% Velocity in the y direction
vy = v(1,2);

end 