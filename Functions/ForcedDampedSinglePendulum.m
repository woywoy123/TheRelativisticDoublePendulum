function [u,uprime] = ForcedDampedSinglePendulum(g,q,theta,thetadot,psi,t)

%Second derivative of theta 
uprime = g*cos(psi*t) - (1/q)*thetadot - sin(theta);

%u is dtheta/dt 
u = thetadot;
end 