function [v,u,vprime,uprime] = DoublePendulum2D(m1,m2,l1,l2,theta,psi,v,u,g)

%The difference in angle 
delta = theta-psi;

%Combined mass 
M = m1 +m2;
%This is the second derivative of psi with respect to time 
uprime = ((m2^2)*l1*l2*(u^2)*sin(delta)*cos(delta)+g*m2*l1*M*sin(theta)*cos(delta)+...
    m2*(l1^2)*M*(v^2)*sin(delta)-m2*l1*M*g*sin(psi))/(M*m2*l2*l1 - (m2^2)*l1*l2*(cos(delta)^2));

%This is the first derivative of psi with time
u = u;

%This is the second derivative of theta with respect to time 
vprime = ((-(m2^2)*l1*l2*(v^2)*sin(delta)*cos(delta)+ (m2^2)*l2*g*sin(psi)*cos(delta)-...
    ((m2*l2*u)^2)*sin(delta)-m2*l2*g*M*sin(theta))/(m2*l2*l1*M - (m2^2)*l1*l2*(cos(delta)^2)));

%This is the first derivative of theta with time
v = v;
end 
