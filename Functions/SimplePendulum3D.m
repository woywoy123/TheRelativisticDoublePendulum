function [u,uprime,x,y,z] = SimplePendulum3D(g,l,t,theta,psistart,thetadot,psidot)

%psi as a function of time 
psi = psidot*t +psistart;

%uprime is the second derivative of theta
uprime = (psidot^2)*cos(theta)*sin(theta)-(g*sin(theta))/l;

%u is dtheta/dt 
u = thetadot;

%conversion from spherical to cartesian___________________________________
%Position as a vector 
r(1,:) = [l*sin(theta)*sin(psi), l*sin(theta)*cos(psi),-l*cos(theta)];
x = r(1,1);
y = r(1,2);
z = r(1,3);

end