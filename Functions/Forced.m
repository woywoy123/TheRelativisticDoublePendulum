clear all;
close all;

t = 0;
h = 0.001*(2*pi)/0.6667;
TEnd = 50*(2*pi)/0.6667;
N = (TEnd -t)/h;
c = 0.5;
omega = 0.6667;
u = 0;
theta = 0;
F = 1.5;

for i = 1:N
    
    t = t + h;
    
    %Runge Kutta
    z1 = u;
    z2 = u + (h/2)*z1;
    z3 = u + (h/2)*z2;
    z4 = u + h*z3;
    theta = theta + (h/6)* (z1 + 2*z2 + 2*z3 +z4);
    
    k1 = -c*u -sin(theta)+F*cos(omega*t);
    k2 = -c*(u+(h/2)*k1) -sin(theta)+F*cos(omega*(t+h/2));
    k3 = -c*(u+(h/2)*k2) -sin(theta)+F*cos(omega*(t+h/2));
    k4 = -c*(u+(h*k3)) -sin(theta)+F*cos(omega*(t+h));
    u = u + (h/6)*(k1+ 2*k2 +2*k3 +k4);
    
    %Phase 
    if theta >= pi 
        theta = theta - 2*pi;
    end 
    if theta <= -pi
        theta = theta + 2*pi;
    end 
    
    plot(theta,u,'r.','markersize',0.0001)
    hold on
    
    
    %Poincare 
%     D = round(cos(omega*t),5);
%     if D == -1
%         plot(theta,u,'r.','markersize',0.0001)
%         hold on
%     elseif D == -0.8
%         plot3(theta,u,-0.8,'b.')
%         hold on
%     elseif D == -0.6
%         plot3(theta,u,-0.6,'m.')
%         hold on
%     elseif D == -0.4
%         plot3(theta,u,-0.4,'c.')
%         hold on
%     elseif D == -0.2
%         plot3(theta,u,-0.2,'r.')
%         hold on
%     elseif D == 0
%         plot3(theta,u,0,'b.')
%         hold on
%     elseif D == 0.2
%         plot3(theta,u,0.2,'m.')
%         hold on
%     elseif D == 0.4
%         plot3(theta,u,0.4,'c.')
%         hold on
%     elseif D == 0.6
%         plot3(theta,u,0.6,'r.')
%         hold on
%     elseif D == 0.8
%         plot3(theta,u,0.8,'b.')
%         hold on
%     elseif D == 1
%         plot3(theta,u,1,'m.')
%         hold on
 %   end 
end 
