function [LP,mP,tP,thetaP,dthetaP] = RelativisticFactors(L,vframe,m0,t,theta,thetadot)

%Constants 
c = 3*10^8; %This is the speed of light in m/s

%The velocity of the bulb inside the moving frame
xdot = vframe(1,1) + L*cos(theta) * thetadot;   %Velocity in the x direction (m/s)
ydot = vframe(1,2) + L*sin(theta) * thetadot;   %Velocity in the y direction (m/s)

%Lorenz factors
gammax = sqrt(1-(xdot/c)^2);    %Gamma in the x direction 
gammay = sqrt(1-(ydot/c)^2);    %Gamma in the y direction 
ratio = gammax/gammay;

%Theta appearing to an outside observer
thetaP = (theta>pi)*(2*pi+ atan(ratio*tan(theta))) + (theta<=pi)*atan(ratio*(tan(theta)));

%The change in theta appearing to an outside observer dthetaP/dt
dthetaP = (1/(1+(ratio*tan(theta))^2))*ratio*((sec(theta))^2)*thetadot;

%The length appearing to an external observer LP
LP = L*cos(theta)/(cos(atan(ratio*tan(theta))));

%The change in Length appearing to an external observer
dLP= cos(theta)/(cos(atan(ratio*tan(theta))));

%The velocity vector appearing to an external observer
vxP = dLP*sin(thetaP) + LP*cos(thetaP)*dthetaP + vframe(1,1);
vyP = dLP*cos(thetaP) - LP*sin(thetaP)*dthetaP + vframe(1,2);
vsquare = vxP^2 + vyP^2;

%Gamma factor
gammafactor = sqrt(1-(vsquare/c^2));

%Mass appearing to the observer
mP = m0/gammafactor;

%Time appearing to the observer
tP = t/gammafactor;
end 




