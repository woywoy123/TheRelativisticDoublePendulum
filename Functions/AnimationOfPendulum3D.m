function [] = AnimationOfPendulum3D(x,y,z,r,Linewidth,stepsize,L1)

%The position of the two objects
P1 = [x, y , z];          %Object 1

%Combining the settings 
pos1 = [P1(1,1) P1(1,2) P1(1,3) r r r];

%Call sphere Matrix
[x y z] = sphere;

%Plotting Surface as mass attached to pendulum 
surf(x*r +P1(1,1), y*r +P1(1,2), z*r +P1(1,3))

%Line Settings
line([-Linewidth Linewidth], [0 0], 'LineWidth',8, 'Color','r')
line([0 (P1(1,1))],[0 (P1(1,2))] ,[0 (P1(1,3))], 'LineWidth',2, 'Color','b')
    
     
        %labeling of Plot
        title('3D Single Pendulum ')
        xlabel('x (m)')
        ylabel('y (m)')
        zlabel('z (m)')
        axis([-L1*1.1 L1*1.1 -L1*1.1 L1*1.1 -L1*1.1 L1*1.1])
        
    %Pausing animation
    el_time = toc(tic());
    pause(stepsize-el_time);
end 