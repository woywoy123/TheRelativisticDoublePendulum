function [] = AnimationOfPendulum(x1,y1,x2,y2,radius1,radius2,L1,L2,Linewidth,stepsize)

%The position of the two objects
P1 = [x1 - radius1/2, y1 - radius1/2];%Object 1
P2 = [x2 - radius2/2, y2-radius2/2]; %Object 2

%Combining the settings 
pos1 = [P1(1,1) P1(1,2) radius1 radius1];
pos2 = [P2(1,1) P2(1,2) radius2 radius2];

cla %clear axis

%Line Settings
line([-Linewidth Linewidth], [0 0], 'LineWidth',8, 'Color','r')
line([0 (P1(1,1)+ radius1/2)],[0 (P1(1,2)+ radius1/2)] , 'LineWidth',2, 'Color','b')
line([(P1(1,1)+ radius1/2),(P2(1,1)+ radius2/2)],[(P1(1,2)+ radius1/2) , (P2(1,2)+ radius2/2)] ...
        , 'LineWidth',2, 'Color','r')
    
%Formating and mass settings
    rectangle('Position',pos1, 'Curvature', [1 1],'FaceColor','b')
    rectangle('Position',pos2, 'Curvature', [1 1],'FaceColor','r')    
   
    %Pausing animation
    el_time = toc(tic());
    pause(stepsize-el_time);
end 